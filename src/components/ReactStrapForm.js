
import { Button, Col, Container, FormGroup, Row } from "reactstrap";
import image from "../../src/assets/images/avatar04.png";

function ReactStrapForm() {
    return (
        <>
            <Container className="container">
                    <Row>
                        <Col sm={12} className="text-center">
                            <h4>HỒ SƠ NHÂN VIÊN</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={8}>
                            <FormGroup>
                                <Row>
                                    <Col sm={3}>
                                        <label>Họ và tên</label>
                                    </Col>
                                    <Col sm={9}>
                                        <input className="form-control" placeholder="Vui lòng điền họ và tên..." />
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Row>
                                    <Col sm={3}>
                                        <label>Số điện thoại</label>
                                    </Col>
                                    <Col sm={9}>
                                        <input className="form-control" placeholder="Vui lòng điền số điện thoại..." />
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Row>
                                    <Col sm={3}>
                                        <label>Ngày sinh</label>
                                    </Col>
                                    <Col sm={9}>
                                        <input className="form-control" placeholder="Vui lòng điền ngày sinh..." />
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Row>
                                    <Col sm={3}>
                                        <label>Giới tính</label>
                                    </Col>
                                    <Col sm={9}>
                                        <input className="form-control" placeholder="Vui lòng điền giới tính..." />
                                    </Col>
                                </Row>
                            </FormGroup>
                        </Col>
                        <Col sm={4} className="text-center">
                            <img src={image} alt="avatar" className="img-thumbnail" />
                        </Col>
                    </Row>
                    <FormGroup>
                        <Row>
                            <Col sm={2}>
                                <label>Công việc</label>
                            </Col>
                            <Col sm={10}>
                                <textarea className="form-control" />
                            </Col>
                        </Row>
                    </FormGroup>
                    <Row>
                        <Col sm={12}>
                            <Button className="details-button" color="info">Chi tiết</Button>
                            <Button color="primary">Kiểm tra</Button>
                        </Col>
                    </Row>
            </Container>
        </>

    )
}

export default ReactStrapForm