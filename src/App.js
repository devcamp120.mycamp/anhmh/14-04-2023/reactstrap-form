import 'bootstrap/dist/css/bootstrap.min.css'
import ReactStrapForm from "./components/ReactStrapForm";

function App() {
  return (
    <div>
      <ReactStrapForm/>
    </div>
  );
}

export default App;
